import Vuex from "vuex";
import {expect} from "chai";
import {config, shallowMount, createLocalVue} from "@vue/test-utils";
import RoutingSliderInputComponent from "../../../components/RoutingSliderInput.vue";
import Routing from "../../../store/indexRouting";

const localVue = createLocalVue();

localVue.use(Vuex);
config.mocks.$t = key => key;

describe("src/modules/tools/routing/components/RoutingSliderInput.vue", () => {
    const mockConfigJson = {
        Portalconfig: {
            menu: {
                tools: {
                    children: {
                        routing:
                            {
                                "name": "translate#common:menu.tools.routing",
                                "glyphicon": "glyphicon-road",
                                "renderToWindow": true
                            }
                    }
                }
            }
        }
    };
    let store,
        wrapper,
        props;

    beforeEach(() => {
        store = new Vuex.Store({
            namespaced: true,
            modules: {
                Tools: {
                    namespaced: true,
                    modules: {
                        Routing
                    }
                }
            },
            state: {
                configJson: mockConfigJson
            }
        });

        props = {
            label: "test",
            value: 3,
            min: 1,
            max: 5,
            step: 1,
            unit: "min",
            disabled: false
        };
    });

    afterEach(() => {
        if (wrapper) {
            wrapper.destroy();
        }
    });

    it("renders RoutingSliderInputComponent", () => {
        wrapper = shallowMount(RoutingSliderInputComponent, {
            store,
            localVue,
            propsData: props
        });
        expect(wrapper.find("#routing-slider-input-test").exists()).to.be.true;
    });
});
